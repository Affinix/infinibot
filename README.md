InfiniBot
=========
An simple discord bot built on Node.js and Discord.js. Command handler from GuideBot

Commands
----------------------------------------------------------------------------------------------------------
| Command             | Description                                | Usage                               |
| ------------------- |:------------------------------------------:| ----------------------------------- |
| i.avatar            | Sends the avatar of the mentioned user     | i.avatar [User]                     |
| i.eval              | Evaluates basic js (Owner Only)            | i.eval [code]                       |
| i.fml               | Returns an random fml from fmylife.com     | i.fml                               |
| i.gif               | Returns an gif from giphy                  | i.gif [search term] [result number] |
| i.invite            | Invite me! :D                              | i.invite                            |
| i.math              | Solves simple math equations               | i.math [equation]                   |
| i.mypermissionlevel | Shows your permission level                | i.mypermissionlevel                 |
| i.ping              | Pong! Checks delay time                    | i.ping                              |
| i.purge             | Clears And certain amount of messages      | i.purge [Messages]                  |
| i.reload            | Reloads an command (Owner Only)            | i.reload [command]                  |
| i.restart           | Restarts the bot (Owner Only)              | i.restart                           |
| i.setprefix         | Sets the bot's prefix for the server       | i.setprefix [prefix]                |
| i.setwelcomemessage | Sets the welcome message for the server    | i.setwelcomemessage [msg]           |
| i.stats             | Shows a few stats about the bot            | i.stats                             |
| i.urban             | Searches the Urban Dictionary for an word  | i.urban [word]                      |
| i.haste             | Uploads code to hastebin                   | i.haste [language] [code]           |
| i.github            | Links you to this page                     | i.github                            |
| i.kick              | Kicks the mentioned person                 | i.kick [user]                       |
| i.sendcode          | Sends The code of the command (Owner Only) | i.sendcode [command]                |
| i.8ball             | Ask the 8ball any question...              | i.8ball [question]                  |
| i.say               | I will say whatever you tell me to         | i.say [msg]                         |
| i.reverse           | Reverses a  given string                   | i.reverse [string]                  |
| i.imdb              | Returns info from imdb about an movie/show | i.imdb [search term]                |
| i.weather           | Returns The weather of an location         | i.weather [location]                |



This bot is still WIP ;D
