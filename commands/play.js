const Discord = require("discord.js");
const ytdl = require('ytdl-core');

exports.run = async (client, message, args) => {

    console.log(args)

    if(!args[0]) 
        return message.channel.send('Please provide an youtube URL!')

    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    var matches = args[0].match(p);

    let url = args[0]

    if(!matches)
        return message.channel.send('That doesnt seem like an valid youtube URL...');

    let voice = message.member.voiceChannel;
    if(!voice) 
        return message.channel.send('You are not in a voice channel!');

    if(!client.queue[message.guild.id]) 
        client.queue[message.guild.id] = {
            queue: [],
            playing: false
        }
    
    let queue = client.queue[message.guild.id].queue;

    queue.push(args[0])

    console.log(queue);

    function play(c, message) {
        queue.dispatcher = c.playStream(ytdl(queue[0], { filter: 'audioonly' }))
        
        queue.shift()

        console.log('should be playing...')
        
        queue.dispatcher.on('end', () => {
            if(queue[0]){
                console.log('end but there is moooorrrreeeeee')
                play(c, message)
            } else c.disconnect()
        })
    }

    if(!message.guild.voiceConnection) 
        voice.join().then(c => {
            play(c, message)
            //c.playStream(ytdl(args[0], { filter: 'audioonly' }))
            console.log('omg it worked you\'re not an complete idiot gg');
        });
    
}

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
  };
  
  exports.help = {
    name: "play",
    category: "Test",
    description: "Testing...",
    usage: "play"
  };