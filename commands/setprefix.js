const Discord = require('discord.js');
const fs = require('fs')
let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))

exports.run = async (client, message, args) => {
    serverConfig[message.guild.id].prefix = args[0]
    fs.writeFileSync("config/serverConfig.json", JSON.stringify(serverConfig, null, 2));
    message.channel.sendEmbed(new Discord.RichEmbed()
        .addField('Success', `My prefix is now ${serverConfig[message.guild.id].prefix}`)
        .setColor(0x5697ff)
    );
}

exports.conf = {
    enabled: true,
    guildOnly: true,
    aliases: [],
    permLevel: 3
  };
  
exports.help = {
    name: "setprefix",
    category: "Setup",
    description: "Sets the prefix to the given prefix",
    usage: "setprefix [prefix]"
};