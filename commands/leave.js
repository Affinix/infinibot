exports.run = async (client, message, args) => {
    const voiceConnection = client.voiceConnections.find(val => val.channel.guild.id == message.guild.id);
    if (voiceConnection === null) return message.channel.send('I\'m not in any channel!');

    //voiceConnection.player.dispatcher.end();
    voiceConnection.disconnect();
}


exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 2
  };
  
  exports.help = {
    name: "leave",
    category: "Test",
    description: "Testing...",
    usage: "leave"
  };