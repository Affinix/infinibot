const Discord = require('discord.js')

exports.run = (client, message, args) => {
    message.channel.sendEmbed(new Discord.RichEmbed()
        .addField('<:github:349831705663897600> Github:', `https://github.com/Affinix/InfiniBot`)
        .setColor(0x5697ff)
    );
}

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['hastebin'],
    permLevel: 0
  };
  
  exports.help = {
    name: "github",
    category: "Info",
    description: "Shows My Discord link",
    usage: "github"
  };