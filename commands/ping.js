const Discord = require('discord.js')

exports.run = async (client, message, args, level) => { 
    await message.channel.sendEmbed(new Discord.RichEmbed()
        .addField('Pong!', `Latency is ${Math.round(client.ping)}ms`)
        .setColor(0x5697ff)
    );
};
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ['p'],
    permLevel: 0
  };
  
  exports.help = {
    name: "ping",
    category: "Miscelaneous",
    description: "Pong! Checks delay time",
    usage: "ping"
  };