const Discord = require('discord.js');
const fs = require('fs')
let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))

exports.run = async (client, message, args) => {
    let args2 = message.content.split(" ").slice(1).join(" ");

    serverConfig[message.guild.id].welcomeMsg = args2
    fs.writeFileSync("config/serverConfig.json", JSON.stringify(serverConfig, null, 2));
    message.channel.sendEmbed(new Discord.RichEmbed()
        .addField('Success', `The welcome message is now ${serverConfig[message.guild.id].welcomeMsg}`)
        .setColor(0x5697ff)
    );
    console.log(serverConfig[message.guild.id].welcomeMsg)
}

exports.conf = {
    enabled: true,
    guildOnly: true,
    aliases: ['setwelcomemsg'],
    permLevel: 2
  };
  
exports.help = {
    name: "setwelcomemessage",
    category: "Setup",
    description: "Sets the welcome message to the given message. {user} = the user that joined",
    usage: "setwelcomemessage [message]"
};