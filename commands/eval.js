Discord = require('discord.js');

exports.run = async (client, message, args, level) => { 
    const code = args.join(" ");
    try {
        const evaled = eval(code);
        const clean = await client.clean(client, evaled);
        if(clean.length > 2000) {
            return message.channel.send('The output was too long to display!')
        }
        message.channel.sendEmbed(new Discord.RichEmbed()
            .addField('Input:', `\`\`\`js\n${code}\n\`\`\``)
            .addField('Output:', `\`\`\`js\n${clean}\n\`\`\``)
            .setColor(0x5697ff)
        );
        message.delete()
    } catch (err) {
        if(err.length > 2000) {
            return message.channel.send('The error was too long to display!')
        }
        message.channel.sendEmbed(new Discord.RichEmbed()
            .addField('Input:', `${code}`)
            .addField('Output:', `\`ERROR\` \`\`\`xl\n${await client.clean(client, err)}\n\`\`\``)
            .setColor(0xff5454)
        );
        message.delete()
    }
  };
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 10 
  };
  
  exports.help = {
    name: "eval",
    category: "System",
    description: "Evaluates arbitrary javascript.",
    usage: "eval [code]"
  };