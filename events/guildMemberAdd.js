const Discord = require('discord.js')
const fs = require('fs');

module.exports = (client, member) => {
    let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))
    if (!serverConfig[member.guild.id].welcomeEnabled === "true") return;

    console.log(serverConfig[member.guild.id].welcomeMsg)

    const welcomeMessage = serverConfig[member.guild.id].welcomeMsg.replace("{user}", member.user.tag);

    console.log(welcomeMessage)

    let channel = member.guild.channels.find("name", serverConfig[member.guild.id].welcomeChannel);
    channel.sendEmbed(new Discord.RichEmbed()
        .addField('Welcome!', `${welcomeMessage}`)
        .setColor(0x5697ff)
    );
}