const fs = require('fs')
let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))

module.exports = (client, guild) => {
    let guildId = guild.id

    if(!serverConfig[guildId])
        serverConfig[guildId] = {
            'prefix' : 'i.',
            'adminRole' : `Owner`,
            'modRole' : 'Mod',
            'defultChannel' : `General`,
            'welcomeMsg' : 'Please welcome {user} to our server! :D',
            'welcomeEnabled' : 'false',
            'welcomeChannel' : 'bots'
        };
}