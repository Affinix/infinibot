const chalkAnimation = require('chalk-animation');

module.exports = async client => {

    await client.wait(1000)

    client.user.setPresence({ game: { name: `with ${client.users.size} users`, type: 0 } });
    console.log('----------------------------------------')
    chalkAnimation.rainbow(`Bot Online! Logged in as ${client.user.tag}, serving ${client.users.size} users in ${client.guilds.size} guilds.`, 2);
    console.log('----------------------------------------')

  };