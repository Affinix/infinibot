const chalkAnimation = require('chalk-animation');
const fs = require('fs');
let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))
const client = new Discord.Client();

module.exports = (client, message) => {
    chalkAnimation.rainbow((message.channel.type === "dm" ? "DM" : message.guild.name) + " : " + message.channel.name + " : " + message.author.username + " : " + message.content); 

    if (message.author.bot) return;
    if (!message.content.startsWith(serverConfig[message.guild.id].prefix)) return;

    const args = message.content.slice(serverConfig[message.guild.id].prefix).trim().split(/ +/g).slice(1);
    let command = message.content.split(" ")[0];
    command = command.slice(serverConfig[message.guild.id].prefix.length).toLowerCase();

    const level = client.permlevel(message);

    const cmd = client.commands.get(command) || client.commands.get(client.aliases.get(command));

    console.log(cmd)
    console.log(command)
    console.log(level)
    
    if(!cmd) return;

    if (cmd && !message.guild && cmd.conf.guildOnly)
        return message.channel.send("This command is unavailable via private message. Please run this command in a guild.");

    if (level >= cmd.conf.permLevel) {
        client.log("log", `${message.author.username} (${message.author.id}) ran command ${cmd.help.name}`, "CMD");
        cmd.run(client, message, args, level);
        
    }
}