const Discord = require("discord.js");
const fs = require('fs')
const { promisify } = require("util");
const readdir = promisify(require("fs").readdir);
const PersistentCollection = require("djs-collection-persistent");
const client = new Discord.Client();

client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
client.config = require("./config.json");
client.queue = {};

require("./modules/functions.js")(client);

const init = async () => {
      const cmdFiles = await readdir("./commands/");
      console.log(`Loading a total of ${cmdFiles.length} commands.`);
      cmdFiles.forEach(f => {
        try {
          const props = require(`./commands/${f}`);
          if (f.split(".").slice(-1)[0] !== "js") return;
          console.log(`Loaded: ${props.help.name}. 👌`);
          client.commands.set(props.help.name, props);
          props.conf.aliases.forEach(alias => {
            client.aliases.set(alias, props.help.name);
          });
        } catch (e) {
          console.log(`Unable to load command ${f}: ${e.stack}`);
        }
      });
    
      const evtFiles = await readdir("./events/");
      console.log(`Loading a total of ${evtFiles.length} events.`);
      evtFiles.forEach(file => {
        const eventName = file.split(".")[0];
        const event = require(`./events/${file}`);
        client.on(eventName, event.bind(null, client));
        delete require.cache[require.resolve(`./events/${file}`)];
        console.log(`loaded ${file}`);
      });

      client.login(client.config.token);
    
};

    init()