const fs = require('fs')

module.exports = (client) => {

    client.permlevel = message => {
        let serverConfig = JSON.parse(fs.readFileSync('config/serverConfig.json', 'utf8'))
        let permlvl = 0;
        let authorID = parseInt(message.author.id)
    
        if (authorID === client.config.ownerID) {
            permlvl = 10;
            return permlvl;
        }
    
        if (!message.guild || !message.member) {
            permlvl = 0;
            return permlvl;
        }


    
        try {
          const modRole = message.guild.roles.find(r => r.name === serverConfig[message.guild.id].modRole);
          if (modRole && message.member.roles.has(modRole.id)) permlvl = 2;
        } catch (e) {
          console.warn("modRole not present in guild settings. Skipping Moderator (level 2) check");
        }
        try {
          const adminRole = message.guild.roles.find(r => r.name === serverConfig[message.guild.id].adminRole);
          if (adminRole && message.member.roles.has(adminRole.id)) permlvl = 3;
        } catch (e) {
          console.warn("adminRole not present in guild settings. Skipping Administrator (level 3) check");
        }
    
        if (message.author.id === message.guild.owner.id) permlvl = 4;
    
        return permlvl;
    };

    client.awaitReply = async (msg, question, limit = 60000) => {
        const filter = m=>m.author.id = msg.author.id;
        await msg.channel.send(question);
        try {
          const collected = await msg.channel.awaitMessages(filter, { max: 1, time: limit, errors: ["time"] });
          return collected.first().content;
        } catch (e) {
          return false;
        }
      };
    
      client.log = (type, msg, title) => {
        if (!title) title = "Log";
        console.log(`[${type}] [${title}]${msg}`);
      };

    client.wait = require("util").promisify(setTimeout);

    process.on("uncaughtException", (err) => {
        const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
        console.error("Uncaught Exception: ", errorMsg);
      });
    
      process.on("unhandledRejection", err => {
        console.error("Uncaught Promise Error: ", err);
      });
    
      
      client.clean = async (client, text) => {
        if (text && text.constructor.name == "Promise")
          text = await text;
        if (typeof evaled !== "string")
          text = require("util").inspect(text, {depth: 0});
    
        text = text
          .replace(/`/g, "`" + String.fromCharCode(8203))
          .replace(/@/g, "@" + String.fromCharCode(8203))
          .replace(client.token, "mfa.VkO_2G4Qv3T--NO--lWetW_tjND--TOKEN--QFTm6YGtzq9PH--4U--tG0");
    
        return text;
      };

}